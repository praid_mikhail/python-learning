#!/usr/bin/env python3

import random


class Character:

    def __init__(self):
        self.health = 1000
        self.damage = 50
        self.state = {}
        self.speed = 100

    def attack(self, enemies):
        skip_turn = False
        for effect in self.__class__.skip_turn_effects:
            if effect in self.state.keys():
                skip_turn = True
        if skip_turn:
            return None, None
        targets = random.sample(enemies, min(self.__class__.attack_targets_number, len(enemies)))
        random_damage_multiplier = random.uniform(0.8, 1.2)
        attack_damage = self.damage * random_damage_multiplier
        for target in targets:
            target.health -= attack_damage
            if target.health <= 0:
                del enemies[enemies.index(target)]
        return [targets, attack_damage]

    def skill(self, enemies):
        skip_turn = False
        for effect in self.__class__.skip_turn_effects:
            if effect in self.state.keys():
                skip_turn = True
        if skip_turn:
            return None, None
        targets = random.sample(enemies, min(self.__class__.skill_targets_number, len(enemies)))
        random_damage_multiplier = random.uniform(0.8, 1.2)
        skill_damage = self.damage * self.__class__.skill_damage_multiplier * random_damage_multiplier
        for target in targets:
            target.health -= skill_damage
            if self.__class__.effects:
                for effect, duration in self.__class__.effects.items():
                    target.state[effect] = [duration, self.effect_damage]
            if target.health <= 0:
                del enemies[enemies.index(target)]
        return [targets, skill_damage]


class Warrior(Character):

    _health_multiplier = 1.3
    _health_grows = 15
    _damage_multiplier = 0.8
    _damage_grows = 2
    _speed_multiplier = 1.2
    _speed_grows = 4
    effects = {'Stun': 1}
    skill_damage_multiplier = 1.0
    _effect_damage_multiplier = 0.0
    attack_targets_number = 2
    skill_targets_number = 1
    skip_turn_effects = ['Stun', 'Disarm']

    def __init__(self, name, level):
        Character.__init__(self)
        self.name = name
        self.level = level
        self.health = self.health * Warrior._health_multiplier + level * Warrior._health_grows
        self.damage = self.damage * Warrior._damage_multiplier + level * Warrior._damage_grows
        self.effect_damage = self.damage * Warrior._effect_damage_multiplier
        self.speed = self.speed * Warrior._speed_multiplier + level * Warrior._speed_grows


class Mage(Character):

    _health_multiplier = 0.9
    _health_grows = 11
    _damage_multiplier = 1.2
    _damage_grows = 3
    _speed_multiplier = 1.1
    _speed_grows = 6
    effects = {}
    skill_damage_multiplier = 1.4
    _effect_damage_multiplier = 0.0
    attack_targets_number = 1
    skill_targets_number = 3
    skip_turn_effects = ['Stun', 'Silence']

    def __init__(self, name, level):
        Character.__init__(self)
        self.name = name
        self.level = level
        self.health = self.health * Mage._health_multiplier + level * Mage._health_grows
        self.damage = self.damage * Mage._damage_multiplier + level * Mage._damage_grows
        self.effect_damage = self.damage * Mage._effect_damage_multiplier
        self.speed = self.speed * Mage._speed_multiplier + level * Mage._speed_grows


class Rogue(Character):

    _health_multiplier = 0.7
    _health_grows = 9
    _damage_multiplier = 1.3
    _damage_grows = 5
    _speed_multiplier = 1.5
    _speed_grows = 10
    effects = {'Poison': 3}
    skill_damage_multiplier = 1.3
    _effect_damage_multiplier = 0.5
    attack_targets_number = 1
    skill_targets_number = 1
    skip_turn_effects = ['Stun', 'Disarm']

    def __init__(self, name, level):
        Character.__init__(self)
        self.name = name
        self.level = level
        self.health = self.health * Rogue._health_multiplier + level * Rogue._health_grows
        self.damage = self.damage * Rogue._damage_multiplier + level * Rogue._damage_grows
        self.effect_damage = self.damage * Rogue._effect_damage_multiplier
        self.speed = self.speed * Rogue._speed_multiplier + level * Rogue._speed_grows


class Ranger(Character):

    _health_multiplier = 1.1
    _health_grows = 13
    _damage_multiplier = 1.1
    _damage_grows = 2.5
    _speed_multiplier = 1.4
    _speed_grows = 7
    effects = {'Disarm': 1}
    skill_damage_multiplier = 1.1
    _effect_damage_multiplier = 0.0
    attack_targets_number = 1
    skill_targets_number = 2
    skip_turn_effects = ['Stun', 'Disarm']

    def __init__(self, name, level):
        Character.__init__(self)
        self.name = name
        self.level = level
        self.health = self.health * Ranger._health_multiplier + level * Ranger._health_grows
        self.damage = self.damage * Ranger._damage_multiplier + level * Ranger._damage_grows
        self.effect_damage = self.damage * Ranger._effect_damage_multiplier
        self.speed = self.speed * Ranger._speed_multiplier + level * Ranger._speed_grows


class Priest(Character):

    _health_multiplier = 0.9
    _health_grows = 11
    _damage_multiplier = 1.3
    _damage_grows = 2
    _speed_multiplier = 1.1
    _speed_grows = 5
    effects = {'Silence': 2}
    skill_damage_multiplier = 1.2
    _effect_damage_multiplier = 0.0
    attack_targets_number = 1
    skill_targets_number = 1
    skip_turn_effects = ['Stun', 'Silence']

    def __init__(self, name, level):
        Character.__init__(self)
        self.name = name
        self.level = level
        self.health = self.health * Priest._health_multiplier + level * Priest._health_grows
        self.damage = self.damage * Priest._damage_multiplier + level * Priest._damage_grows
        self.effect_damage = self.damage * Priest._effect_damage_multiplier
        self.speed = self.speed * Priest._speed_multiplier + level * Priest._speed_grows


def battle(darkness, light):
    step = 1
    while darkness and light:
        print("Turn №" + str(step))
        darkness, light = turn(darkness, light, step)
        step += 1
    print("Darkness wins!" if darkness else "Light wins!")


def print_battlefield(darkness, light, targets=None, attacker=None, taken_damage=0):
    print('______________________________________________')
    print('Darkness:')
    for character in darkness:
        print('{} {:.2f}'.format(character.name, character.health), end=' ')
        if targets:
            if character in targets:
                print('({}: -{:.2f})'.format(attacker, taken_damage), end=' ')
        for effect, (duration, damage) in character.state.items():
            print('({} : {} turns, {:.2f} damage)'.format(effect, duration, damage), end='')
        print()
    print()
    print('Light:')
    for character in light:
        print('{} {:.2f}'.format(character.name, character.health), end=' ')
        if targets:
            if character in targets:
                print('({}: -{:.2f})'.format(attacker, taken_damage), end=' ')
        for effect, (duration, damage) in character.state.items():
            print('({} : {} turns, {:.2f} damage)'.format(effect, duration, damage), end='')
        print()
    print()
    print('______________________________________________')


def turn(darkness, light, step):

    print('Before turn:')
    print_battlefield(darkness, light)

    darkness, light = damaging_phase(darkness, light, step)
    darkness, light = effects_phase(darkness, light)

    print('End turn:')
    print_battlefield(darkness, light)
    return darkness, light


def damaging_phase(darkness, light, step):
    turn_order = darkness[:]
    turn_order.extend(light)
    turn_order.sort(key=lambda character: character.speed)
    while turn_order:
        character = turn_order.pop()
        if character in darkness:
            if step % 3 == 0:
                targets, damage = character.skill(light)
            else:
                targets, damage = character.attack(light)
        else:
            if step % 3 == 0:
                targets, damage = character.skill(darkness)
            else:
                targets, damage = character.attack(darkness)
        attacker = character.name
        if targets:
            for target in targets:
                if (target in turn_order
                        and target.health <= 0):
                    del turn_order[turn_order.index(target)]
        if damage is not None:
            print_battlefield(darkness, light, targets, attacker, damage)
    return darkness, light


def effects_phase(darkness, light):
    for character in darkness:
        for key in character.state.keys():
            character.state[key][0] -= 1
            if key == 'Poison':
                character.health -= character.state[key][1]
        character.state = {k: v for k, v in character.state.items() if v[0] > 0}
    darkness = [character for character in darkness if character.health >= 0]
    for character in light:
        for key in character.state.keys():
            character.state[key][0] -= 1
            if key == 'Poison':
                character.health -= character.state[key][1]
        character.state = {k: v for k, v in character.state.items() if v[0] > 0}
    light = [character for character in light if character.health >= 0]
    return darkness, light


a = Warrior('Warrior', 53)
b = Ranger('Ranger', 47)
c = Priest('Priest', 41)
d = Rogue('Rogue', 41)
e = Mage('Mage', 39)
f = Priest('Dark Priest', 47)
g = Mage('Fire Mage', 45)
h = Rogue('Cruel Rogue', 50)
i = Warrior('Tanky', 51)
j = Ranger('Sharpshooter', 49)
battle([c, h, e, b, a], [f, d, g, i, j])
