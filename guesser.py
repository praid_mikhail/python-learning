#!/usr/bin/env python3
import random

message = """Hi! This is the 'Guess the number' game! We'll start in a second!"""
print(message)
number = random.randint(-100, 100)
rules = """Let me explain explain you the rules. You must guess the number in range between -100 and 100. I'll help you. But don't trust me every 5th turn. Enter 'Quit' to quit."""
print(rules)
print("Let the game begin!!!")
turn = 1
win = False
while True:
    string = input("Enter your guess: ")
    if string.lower() == "quit":
        break
    else:
        guess = int(string)
    if turn % 5 == 0:
        islie = True
    else:
        islie = False
    if islie:
        print("Ahaha, you'll never guess it!!!")
    else:
        if guess == number:
            print("Oh, year! You've guessed it!")
            win = True
            break
        elif guess > number:
            print("No, it is lower.")
        else:
            print("No, it is higher.")
    turn += 1
if win:
    print("It took you", turn, "turn(s) to win!!! Congratulations!!!")
else:
    print("Goodbye, better luck next time!")
