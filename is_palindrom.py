#!/usr/bin/env python3

def is_palindrome(string):
    punct_marks = [' ', '!', ',', '.', '"', "'", '?', '-', ':', ';']
    for char in string:
        if char in punct_marks:
            string = string.replace(char, '')
    string = string.lower()
    return string == string[::-1]

string = input('Enter your string: ')
if is_palindrome(string):
    print('Your string is a palindrome!')
else:
    print('This string seems to be not a palindrome!')

